function domID(id) {
  return document.getElementById(id);
}
// Bài 1
document.getElementById("btnBai1").onclick = function () {
  var diemThu1 = domID("diemThu1").value * 1;
  var diemThu2 = domID("diemThu2").value * 1;
  var diemThu3 = domID("diemThu3").value * 1;
  var diemTong3Mon = diemThu1 + diemThu2 + diemThu3;
  var diemChuan = domID("diemChuan").value * 1;
  var khuVuc = domID("khuVuc").value;
  var doiTuong = domID("doiTuong").value;
  var diemCongKhuVuc = 0;
  var diemCongDoiTuong = 0;
  var ketQuaThi = "";
  if (khuVuc == "A") {
    diemCongKhuVuc = 2;
  } else if (khuVuc == "B") {
    diemCongKhuVuc = 1;
  } else if (khuVuc == "C") {
    diemCongKhuVuc = 0.5;
  }
  if (doiTuong == 1) {
    diemCongDoiTuong = 2.5;
  } else if (doiTuong == 2) {
    diemCongDoiTuong = 1.5;
  } else if (doiTuong == 3) {
    diemCongDoiTuong = 1;
  }
  var diemTong = diemTong3Mon + diemCongKhuVuc + diemCongDoiTuong;

  if (diemThu1 <= 0 || diemThu2 <= 0 || diemThu3 <= 0) {
    ketQuaThi = "Bạn đã rớt do có môn điểm nhỏ hơn hoặc bằng 0";
  } else if (diemTong >= diemChuan) {
    ketQuaThi = "Chúc mừng bạn đã đậu";
  } else if (diemTong < diemChuan) {
    ketQuaThi = "Bạn đã rớt";
  }
  domID(
    "ketQuaBai1"
  ).innerText = `Tổng điểm của bạn là ${diemTong}. ${ketQuaThi} `;
};
// Bài 2
domID("btnTinhTienDien").onclick = function () {
  var hoTen = domID("hoTen").value;
  var soDien = domID("soKw").value * 1;
  function soTien50KwDau(soKy) {
    var tien = soKy * 500;
    return tien;
  }
  function soTien50KwKe(soKy) {
    var tien = 50 * 500 + (soKy - 50) * 650;
    return tien;
  }
  function soTien100KwKe(soKy) {
    var tien = 50 * 500 + 50 * 650 + (soKy - 100) * 850;
    return tien;
  }
  function soTien150Kwke(soKy) {
    var tien = 50 * 500 + 50 * 650 + 100 * 850 + (soKy - 200) * 1100;
    return tien;
  }
  function soTienSau350Kw(soKy) {
    var tien =
      50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (soKy - 350) * 1300;
    return tien;
  }
  var soTienDien = 0;
  if (soDien <= 50) {
    soTienDien = soTien50KwDau(soDien).toLocaleString();
  } else if (soDien <= 100) {
    soTienDien = soTien50KwKe(soDien).toLocaleString();
  } else if (soDien <= 200) {
    soTienDien = soTien100KwKe(soDien).toLocaleString();
  } else if (soDien <= 350) {
    soTienDien = soTien150Kwke(soDien).toLocaleString();
  } else if (soDien > 350) {
    soTienDien = soTienSau350Kw(soDien).toLocaleString();
  }
  domID(
    "ketQuaBai2"
  ).innerText = `Họ tên : ${hoTen} ; Tiền điện : ${soTienDien} VNĐ `;
};
//Bài 3
domID("btnBai3").onclick = function () {
  var thuNhapNam = domID("tongThuNhapNam").value * 1;
  var nguoiPhuThuoc = domID("soNguoiPhuThuoc").value * 1;
  var hoTen = domID("hoTenBai3").value;
  // var thuNhap = 0
  function thuNhapDuoi60tr(thunhap) {
    var tien = thunhap * 0.05;
    return tien;
  }
  function thuNhapTu60Den120(thunhap) {
    var tien = 60e6 * 0.05 + (thunhap - 60e6) * 0.1;
    return tien;
  }
  function thuNhapTu120Den210(thunhap) {
    var tien = 60e6 * 0.05 + 60e6 * 0.1 + (thunhap - 120e6) * 0.15;
    return tien;
  }
  function thuNhapTu210Den384(thunhap) {
    var tien = 60e6 * 0.05 + 60e6 * 0.1 + 90e6 * 0.15 + (thunhap - 210e6) * 0.2;
    return tien;
  }
  function thuNhapTu384Den624(thunhap) {
    var tien =
      60e6 * 0.05 +
      60e6 * 0.1 +
      90e6 * 0.15 +
      174e6 * 0.2 +
      (thunhap - 384e6) * 0.25;
    return tien;
  }
  function thuNhapTu624Den960(thunhap) {
    var tien =
      60e6 * 0.05 +
      60e6 * 0.1 +
      90e6 * 0.15 +
      174e6 * 0.2 +
      240e6 * 0.25 +
      (thunhap - 624e6) * 0.3;
    return tien;
  }
  function thuNhapTren960(thunhap) {
    var tien =
      60e6 * 0.05 +
      60e6 * 0.1 +
      90e6 * 0.15 +
      174e6 * 0.2 +
      240e6 * 0.25 +
      336e6 * 0.3 +
      (thunhap - 960e6) * 0.35;
    return tien;
  }
  var thuNhapChiuThue = thuNhapNam - 4e6 - nguoiPhuThuoc * 16e5;
  var thuePhaiNop = 0;
  if (thuNhapChiuThue < 0) {
    thuePhaiNop = 0;
  } else if (thuNhapChiuThue <= 60e6) {
    thuePhaiNop = new Intl.NumberFormat("de-DE", {
      style: "currency",
      currency: "VND",
    }).format(thuNhapDuoi60tr(thuNhapChiuThue));
  } else if (thuNhapChiuThue <= 120e6) {
    thuePhaiNop = new Intl.NumberFormat("de-DE", {
      style: "currency",
      currency: "VND",
    }).format(thuNhapTu60Den120(thuNhapChiuThue));
  } else if (thuNhapChiuThue <= 210e6) {
    thuePhaiNop = new Intl.NumberFormat("de-DE", {
      style: "currency",
      currency: "VND",
    }).format(thuNhapTu120Den210(thuNhapChiuThue));
  } else if (thuNhapChiuThue <= 384e6) {
    thuePhaiNop = new Intl.NumberFormat("de-DE", {
      style: "currency",
      currency: "VND",
    }).format(thuNhapTu210Den384(thuNhapChiuThue));
  } else if (thuNhapChiuThue <= 624e6) {
    thuePhaiNop = new Intl.NumberFormat("de-DE", {
      style: "currency",
      currency: "VND",
    }).format(thuNhapTu384Den624(thuNhapChiuThue));
  } else if (thuNhapChiuThue <= 960e6) {
    thuePhaiNop = new Intl.NumberFormat("de-DE", {
      style: "currency",
      currency: "VND",
    }).format(thuNhapTu624Den960(thuNhapChiuThue));
  } else if (thuNhapChiuThue > 960e6) {
    thuePhaiNop = new Intl.NumberFormat("de-DE", {
      style: "currency",
      currency: "VND",
    }).format(thuNhapTren960(thuNhapChiuThue));
  }
  domID(
    "ketQuaBai3"
  ).innerText = `Họ tên : ${hoTen} ; Tiền thuế thu nhập cá nhân: ${thuePhaiNop} `;
};
// Bài 4
function loaiKhachHang() {
  var doiTuong = domID("chonLoaiKH").value;
  if (doiTuong == "doanhNghiep") {
    domID("soLuongKetNoi").style.display = "block";
  } else {
    domID("soLuongKetNoi").style.display = "none";
  }
}
domID("btnBai4").onclick = function () {
  //input
  var doiTuong = domID("chonLoaiKH").value;
  var maKH = domID("maKhachHang").value;
  var soKenh = domID("soKenh").value * 1;
  var soKetNoi = domID("soLuongKetNoi").value * 1;
  const phiHoaDonDan = 4.5 * 1;
  const phiHoaDonDoanhNghiep = 15 * 1;
  const phiDVCoBanDan = Number(20.5);
  const thueKenhDan = 7.5 * 1;
  const thueKenhDN = 50 * 1;

  // output: number
  var tienCap = 0;
  if (doiTuong == "") {
    alert("Vui lòng chọn loại khách hàng");
  } else if (doiTuong == "nhaDan") {
    tienCap = (
      phiHoaDonDan +
      phiDVCoBanDan +
      thueKenhDan * soKenh
    ).toLocaleString();
  } else if (doiTuong == "doanhNghiep") {
    if (soKetNoi <= 10) {
      tienCap = (
        phiHoaDonDoanhNghiep +
        thueKenhDN * soKenh +
        75
      ).toLocaleString();
    } else {
      tienCap = (
        phiHoaDonDoanhNghiep +
        thueKenhDN * soKenh +
        75 +
        (soKetNoi - 10) * 5
      ).toLocaleString();
    }
  }
  domID(
    "ketQuaBai4"
  ).innerText = `Mã khách hàng : ${maKH} ; Tiền cáp: $${tienCap} `;
};
